import React, { Component } from 'react';
var setUrl="";


function compare_qty(a, b){
  // a should come before b in the sorted order
  if(a.noTransacciones > b.noTransacciones){
          return -1;
  // a should come after b in the sorted order
  }else if(a.noTransacciones < b.noTransacciones){
          return 1;
  // a and b are the same
  }else{
          return 0;
  }
}

class Transaccion extends Component {
  constructor() {
    super();
    this.state = {
      todos: []
    }
    setUrl ='/monitor/getTransacciones';
    console.log("ObteniendoTransacciones")
    console.log(setUrl)
    fetch(setUrl)
    .then(res => res.json())
    .then((data) => {
      console.log("dataA******************************")
      data.reg.sort(compare_qty)
      this.setState({ todos: data.reg})
      console.log(this.state.todos)
    })
    .catch(console.log)

   
  
  }
  
  


  render() {
    const todos = this.state.todos.map((todo, i) => {
      return (
        <div className="col-md-4" >
           
          <div className="card mt-4" key={i}>
          <div class="card-header">Top {i+1}</div>
            <div className="card-body">
            <table class="table table-hover">
              <tbody>
                <tr className="table-active">
                  <th scope="row">Nombre</th>
                  <td>{todo.nombre}</td>
                </tr>
                <tr>
                  <th scope="row">Telefono</th>
                  <td> {todo.telefono}</td>
                </tr>
                <tr className="table-active">
                  <th scope="row">Transacciones</th>
                  <td>{todo.noTransacciones}</td>
                </tr>
              </tbody>
            </table>

            </div>
          </div>
        

        </div>
      )
    });
    return (
      <div className="App">

        <nav className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="/">
            Top de cantidad de Transacciones

          </a>
        </nav>




              <div className="row">
                {todos}
              </div>


      </div>
    );
  }


  
}

export default Transaccion;