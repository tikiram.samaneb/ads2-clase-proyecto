import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
var setUrl="";



function compare_qty(a, b){
  // a should come before b in the sorted order
  if(a.noTransacciones > b.noTransacciones){
          return -1;
  // a should come after b in the sorted order
  }else if(a.noTransacciones < b.noTransacciones){
          return 1;
  // a and b are the same
  }else{
          return 0;
  }
}




class Transferencias extends Component {
  constructor() {
    super();
    this.state = {
      todos: [],
      monto:'100000000'
    }

      setUrl ='/monitor/getTransferencias?limite='+this.state.monto;
      console.log("ObteniendoTransacciones")
      console.log(setUrl)
      fetch(setUrl)
      .then(res => res.json())
      .then((data) => {
        console.log("dataA******************************")
        data.reg.sort(compare_qty)
        this.setState({ todos: data.reg})
        console.log(this.state.todos)
      })
      .catch(console.log)

      this.getRecargas = this.getRecargas.bind(this);
  }
  
  
  getRecargas(){
    setUrl ='/monitor/getTransferencias?limite='+this.state.monto;
    console.log("ObteniendoTransacciones")
    console.log(setUrl)
    fetch(setUrl)
    .then(res => res.json())
    .then((data) => {
      console.log("dataA******************************")
      data.reg.sort(compare_qty)
      console.log(data.reg)
      this.setState({ todos: data.reg})
      console.log(this.state.todos)
    })
    .catch(console.log)
  }





  render() {
    const todos = this.state.todos.map((todo, i) => {
      return (
        <div className="col-md-4" >
           
          <div className="card mt-4" key={i}>
          <div class="card-header">Transferencia No. {i+1}</div>
            <div className="card-body">
            <table class="table table-hover">
              <tbody>
                <tr className="table-active">
                  <th scope="row">Nombre</th>
                  <td>{todo.nombre}</td>
                </tr>
                <tr>
                  <th scope="row">Telefono</th>
                  <td> {todo.telefono}</td>
                </tr>
                <tr className="table-active">
                  <th scope="row">Monto</th>
                  <td>{todo.monto}</td>
                </tr>
                <tr>
                  <th scope="row">Fecha</th>
                  <td> {todo.fecha}</td>
                </tr>
                <tr className="table-active">
                  <th scope="row">Descripcion</th>
                  <td>{todo.descripcion}</td>
                </tr>
              </tbody>
            </table>

            </div>
          </div>
        

        </div>
      )
    });
    return (
      <div className="App">

        <nav className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="/">
            Transferencia
          </a>
          <Form inline>
            <FormControl type="text" placeholder="Ingrese monto a buscar" className="textFeedback" onChange={e => this.setState({ monto: e.target.value })} />
            <Button variant="outline-info"onClick={this.getRecargas} >Buscar</Button>
          </Form>
        </nav>




              <div className="row">
                {todos}
              </div>


      </div>
    );
  }


  
}

export default Transferencias;