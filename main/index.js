const express = require('express');
var request = require('request');
const bodyParser = require("body-parser");
const jsonParser  = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false })
const cors = require('cors');
const app = express();
app.use(cors());
const port = 80;

/**********************************************
 *          INICIAN RUTAS DE AGENTES          *
 **********************************************/
const urlAgentes = 'agents-api-container/agents';
app.get('/agents/', async function(req, res) {
    var opts = {
        uri: `http://${urlAgentes}/`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.get("/agents/:id", async function(req, res) {
    const id = req.params.id;
    var opts = {
        uri: `http://${urlAgentes}/${id}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.post('/agents/', jsonParser, function(req, res) {
    console.log({body: req.body});
    var opts = {
        uri: `http://${urlAgentes}/`,
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.delete('/agents/:id', async function(req, res) {
    const id = req.params.id;
    var opts = {
        uri: `http://${urlAgentes}/${id}`,
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.patch('/agents/:id', jsonParser, function(req, res) {
    const id = req.params.id;
    var opts = {
        uri: `http://${urlAgentes}/${id}`,
        method: 'PATCH',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

/**********************************************
 *         FINALIZAN RUTAS DE AGENTES         *
 **********************************************/
/**********************************************
 *          INICIAN RUTAS DE MONITOR          *
 **********************************************/
const urlMonitor = 'microservicio_monitor';
app.get('/monitor/prueba', async function(req, res) {
    let qs = '?';
    let amp = '';
    for (const key in req.query) {
        qs += `${amp}${key}=${req.query[key]}`;
        amp = '&';
    }
    var opts = {
        uri: `http://${urlMonitor}/prueba${qs}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.get('/monitor/getTransacciones', async function(req, res) {
    let qs = '?';
    let amp = '';
    for (const key in req.query) {
        qs += `${amp}${key}=${req.query[key]}`;
        amp = '&';
    }
    var opts = {
        uri: `http://${urlMonitor}/getTransacciones${qs}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.get('/monitor/getRecargas', async function(req, res) {
    let qs = '?';
    let amp = '';
    for (const key in req.query) {
        qs += `${amp}${key}=${req.query[key]}`;
        amp = '&';
    }
    var opts = {
        uri: `http://${urlMonitor}/getRecargas${qs}`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.get('/monitor/getTransferencias', async function(req, res) {
    let qs = '?';
    let amp = '';
    for (const key in req.query) {
        qs += `${amp}${key}=${req.query[key]}`;
        amp = '&';
    }
    var opts = {
        uri: `http://${urlMonitor}/getTransferencias${qs}`,
        method: 'GET',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

/**********************************************
 *         FINALIZAN RUTAS DE MONITOR         *
 **********************************************/
/**********************************************
 *            INICIAN RUTAS DE INFO           *
 **********************************************/
const urlInfo = 'client_infouser';
app.get('/info/', async function(req, res) {
    var opts = {
        uri: `http://${urlInfo}/`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.post('/info/search', urlencodedParser, function(req, res) {
    var opts = {
        uri: `http://${urlInfo}/search`,
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});

app.post('/info/activar', urlencodedParser, function(req, res) {
    var opts = {
        uri: `http://${urlInfo}/activar`,
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        return res.send(response.body);
    });
});
/**********************************************
 *           FINALIZAN RUTAS DE INFO          *
 **********************************************/
app.use('/monitor', express.static('monitor'));
app.use('/agentes', express.static('agentes'));
app.all('/agentes*', function (req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('./agentes/index.html', { root: __dirname });
});


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})