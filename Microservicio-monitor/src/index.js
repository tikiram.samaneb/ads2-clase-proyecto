
const express = require('express');
const routes = require ('./rutas/routes'); // aqui se manejara cualquier peticion del monitor (frontend)

const puerto = process.env.PORT || 2000; 

const cors = require('cors');
const app = express();
    app.use(cors());
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use('/', routes);

    
app.listen(puerto, ()=>{
    console.log("Servidor funcionando en el puerto "+ puerto);
});
