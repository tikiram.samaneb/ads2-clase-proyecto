

const express = require('express');
const router = express.Router();
const controlador = require('../controlador/controlador');

// PETICIONES HTTP a nuestro microservicio por parte del frontend
router.get('/prueba', controlador.prueba);
router.get('/getTransacciones', controlador.transacciones); 
router.get('/getRecargas', controlador.recargas); // billeteras que hayan recargado mas de cierto parametro
router.get('/getTransferencias', controlador.transferencias); // billeteras que hayan transferido mas de cierto parametro

module.exports = router;