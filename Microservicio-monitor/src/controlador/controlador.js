const axios = require('axios');

let obj = [
    {telefono:"55698659", tipo:"1", monto:"10000", fecha:"2020-10-29"},
    {telefono:"55698659", tipo:"1", monto:"10001", fecha:"2020-10-29"},
    {telefono:"55698659", tipo:"3", monto:"345.20", fecha:"2020-10-29"},
    {telefono:"55698659", tipo:"4", monto:"50000", fecha:"2020-10-29"},
    {telefono:"55698659", tipo:"1", monto:"345.20", fecha:"2020-10-29"}
];
const prueba = (req, res)=>{

   // res.send("Si te contestamos desde el controlador");
    axios_GET('http://pokeapi.co/api/v2/pokemon/').then( (resp) =>{
        console.log(resp);
        res.send(resp);
    });
    
}


async function axios_GET(url){

    let resp = {status:'', registros:[]} ; // almacena el codigo y los registros
   
    await axios.get(url).then( (r)=>{
        //console.log(r);
        resp.status    = r.status
        resp.registros = r.data;

    }).catch( (error) =>{
        //console.log(error);
        console.log("Error al realizar GET (microservicio seguira funcionando correctamente)");
        resp.status = error.response.status;
       // console.log(error['response'].status);
    });
    
    return resp;
}

const transacciones = (req, res)=>{ // FRONTEND desea consultar transacciones [ ip-adress:3001/reportetotal ]

    axios_GET('http://52.14.227.185:3001/reporteTotal').then( (resp) =>{
       // console.log(resp);
        //console.log(resp.registros.length);
        switch(resp.status)
        {
            case 200:
            {
                let pivote;
                let transacciones = resp.registros;
                let val_cotejados = [];
                let newArray = [];
                
                for(let i =0; i < (transacciones.length); i++){
                    let contador = 0;
                    pivote = transacciones[i];
                    val_cotejados.includes()
                    if(!(val_cotejados.includes(pivote.telefono)) ){ // si ese valor aun no fue contado
                    
                        for(let j = 0; j < transacciones.length; j++){
                            if(pivote.telefono === transacciones[j].telefono)
                                contador++;
                        }
                        //console.log('telefono ->',pivote.telefono, ':', contador);
                        val_cotejados.push(pivote.telefono);
                        newArray.push( {telefono: pivote.telefono, nombre:pivote.nombre,noTransacciones:contador} );
                    }
                
                }
        
                res.send(JSON.stringify({codigo:1, reg:newArray} ));
                console.log(newArray);
                
            }break;

            case 404:{
                res.send(JSON.stringify({codigo:3, reg:[ {msg:'Error interno en la billetera'} ]}));
            }break;
        }
        
    });
}



const recargas = (req, res)=>{ // FRONTEND desea consultar Billeteras mayores a cierto parametro [ ip-adress:3001/reportetotal ]

    let {limite} = req.query; 
    let limFloat = parseFloat(limite);
    
    //let respuesta_interna = {codigo:-1, reg:[] };
    if(Number.isInteger(limFloat))
    {
        axios_GET('http://52.14.227.185:3001/reporteTotal').then( (resp) => {
            //console.log(resp);
            switch(resp.status){

                case 200:{
                    let hoy = new Date();
                    
                    let newArray = resp.registros.filter(elemento => {
                        
                        let fel = new Date(elemento.fecha); // fecha del elemento
                        return ( (parseFloat(elemento.monto) >= limFloat)&&(elemento.tipo =='0')
                            &&((hoy.getDate() === fel.getDate()) && (hoy.getMonth() === fel.getMonth())&&(hoy.getFullYear()===fel.getFullYear())) );
                    });
                    //console.log(newArray);
                    res.send(JSON.stringify({codigo:1, reg:newArray} ));

                }break;
                case 404:{ // si se cae el servidor de la billetera
                    res.send(JSON.stringify({codigo:3, reg:[ {msg:'Error interno en la billetera'} ]}));
                }break;
            }  
        });
    }else{
        res.send(JSON.stringify( {codigo:2, reg:[]} ));
    }    
}


const transferencias = (req, res)=>{
    
    let {limite} = req.query;
    var limiteFloat = parseFloat(limite);
    
    if(!isNaN(limiteFloat)){

        axios_GET('http://52.14.227.185:3001/reporteTotal').then( (resp) => {

            switch(resp.status){

                case 200:{
                    let hoy = new Date(); // 2020-11-04T03:57:53.000Z - 2020-11-04T03:55:50.000Z

                    let newArray = resp.registros.filter(elemento =>{

                        let fel = new Date(elemento.fecha); // fecha del elemento 
                        
                        return ( (parseFloat(elemento.monto) >= limiteFloat) 
                            && ( (elemento.tipo == '2' || elemento.tipo=='3'|| elemento.tipo=='4'||elemento.tipo=='5') )
                            && ( (hoy.getDate() === fel.getDate()) && (hoy.getMonth() === fel.getMonth())&&(hoy.getFullYear()===fel.getFullYear()) ) );
                    });
                    //console.log(newArray);
                    res.send(JSON.stringify({codigo:1, reg:newArray} )); 

                }break;
                case 404:{ // si se cae el servidor de la billetera
                    res.send(JSON.stringify({codigo:3, reg:[]}));
                }break;
            }  
        });
    }else{
        res.send(JSON.stringify({codigo:2, reg:[ {msg:'El parametro solo puede contener valores de tipo (NUMERICO)'} ]}));
    }
    
}
 

module.exports={
    prueba:prueba,
    transacciones:transacciones,
    recargas:recargas,
    transferencias:transferencias
}