const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');

const app = express();

// Handlebars middleware
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.set('views', path.join(__dirname, 'views'));

//body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

//index route
app.get('/', (req, res) => {
    res.render('user');
});

// routes
app.use('/', require('./routes/infoUser'));

const port = process.env.PORT || 5000;

app.listen(port, console.log('Servidor corriendo en puerto '+port));