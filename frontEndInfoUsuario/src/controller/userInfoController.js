const controller = {};
const https = require('http');
const host = process.env.HOST || 'localhost';
const port = process.env.PORT_2 || 3000;

controller.search = (req, res) => {
    const { number } = req.body;
    let errors = [];
    let dataString = '';
    let nombre, apellido, fechaNac, email, dpi;
    console.log(req.body);
    if (!number) {
        errors.push({
            text: 'Por Favor Ingrese Número Telefónico'
        });
    }

    if (errors.length > 0) {
        res.render('user', {
            errors
        });
    } else {
        var direccion = 'http://' + host + ':' + port + '/getUserInfo/user?cell_phone=' + number;
        console.log(direccion);

        https.get(direccion, function (resp) {
            resp.on('data', chunk => {
                dataString += chunk;
            });
            resp.on('end', () => {
                const json = JSON.parse(dataString);
                console.log({ info: json.info });
                console.log({ report: json.report });
                const info = JSON.parse(json.info);
                const report = JSON.parse(json.report);

                var nombreComp = info.nombre ? info.nombre.split(" ") : '';
                nombre = nombreComp[0];
                apellido = nombreComp[1];
                fechaNac = info.nacimiento;
                email = info.correo;
                dpi = info.dpi;
                let i = 1;
                const transacciones = (!report.mensaje) ? report.map(transaccion => {
                    function getTipo(tipo){
                        switch (tipo){
                            case 0:
                                return 'Recarga';
                            case 1:
                                return 'Retiro';
                            case 2:
                                return 'Credito a tercero';
                            case 3:
                                return 'Débito a tercero';
                            case 4:
                                return 'Pago Luz';
                            case 5:
                                return 'Pago Agua';
                            default:
                                return 'Tipo desconocido ' + tipo;
                        }
                    }
                    return {
                        no: i++,
                        fechaTrans: transaccion.fecha,
                        montoTrans: transaccion.monto,
                        DescripTrans: transaccion.descripcion,
                        tipoTrans: getTipo(transaccion.tipo),
                    }
                }) : [];
                console.log(nombre);
                console.log(apellido);
                console.log(fechaNac);
                console.log(email);
                console.log(dpi);


                res.render('user', {
                    nombre,
                    apellido,
                    fechaNac,
                    email,
                    dpi,
                    telefono: number,
                    transacciones,
                });
            });
        });

        /*
        https.get(direccion, function (res) {
            res.on('data', chunk => {
                dataString += chunk;
            });
            res.on('end', () => {
                const json = JSON.parse(dataString);

                let pos = 0;
                try {
                    var items = Object.keys(json[pais]);
                    let bandera = false;
                    let valor;

                    items.forEach(function (item) {
                        var value = json[pais][item];
                        if (date === value.date) {
                            console.log(pais + ': ' + item + ' = ' + value.date);
                            bandera = true;
                            valor = value;
                        }
                    });

                    mensaje = 'No se encuentra fecha';

                } catch (error) {
                    mensaje = 'No se encuentra pais';
                }

            });
        });
        */
    }
}


module.exports = controller;