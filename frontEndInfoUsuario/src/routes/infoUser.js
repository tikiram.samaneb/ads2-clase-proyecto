const express = require('express');
const router = express.Router();
const request = require('request')

const host = process.env.HOST || 'localhost';
const port = process.env.PORT_2 || 3000;

const user_controller = require('../controller/userInfoController');

router.post('/search', user_controller.search);
router.post('/activar',  (req, res) => {
    const { estado, telefono } = req.body;
    let errors = [];
    let dataString = '';
    let nombre, apellido, fechaNac, email, dpi;
    console.log(req.body);
    if (!telefono) {
        errors.push({
            text: 'Por Favor Ingrese Número Telefónico'
        });
    }

    if (errors.length > 0) {
        res.render('user', {
            errors
        });
    } else {
        var direccion = `http://${host}:${port}/activar?telefono=${telefono}&estado=${estado}`;
        console.log(direccion);


        request.get(direccion, function (resp) {
            console.log({resp});
            res.render('user');
            // resp.on('data', chunk => {
            //     dataString += chunk;
            // });
            // resp.on('end', () => {
            //     const json = JSON.parse(dataString);
            //     console.log({ json: json });

            //     res.render('user', {
            //         msgActiv: json.mensaje,
            //     });
            // });
        });
    }
});

module.exports = router;