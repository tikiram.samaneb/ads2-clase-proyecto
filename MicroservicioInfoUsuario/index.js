require('dotenv').config();

const express = require("express");
const cors = require('cors');
const port = process.env.PORT || 3000;
const app = express();

var users = require('./src/Routes/routes')

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/', users);

app.listen(4000, () => {
    console.log("El servidor está inicializado en el puerto 4000");
   });