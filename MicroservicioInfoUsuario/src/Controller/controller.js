const request = require('request');

const getUserInfo = (req, res) => { // GET (id especifico)

    //console.log(req.query)
    const { cell_phone } = req.query;
    //console.log(cell_phone);
    request('http://52.14.227.185:3003/obtenerUsuario?telefono=' + cell_phone, (err, response, body) => {

        if (err) {
            return console.log(err);
        } else {
            request('http://52.14.227.185:3001/reporte?telefono=' + cell_phone, (err2, response2, body2) => {
                if (err) {
                    res.send({ info: body });
                } else {
                    res.send({ info: body, report: body2 });
                }
            });
        }

    });
};
const cambioEstado = (req, res) => {
    const { telefono, estado } = req.query;
    
    console.log({body: req.body});
    var opts = {
        uri: `http://52.14.227.185:3000/cambioEstado`,
        method: 'POST',
        body: JSON.stringify({ telefono, estado }),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    console.log({opts});
    request(opts, function (error, response) {
        if (error) { return console.log(err); }
        return res.send(response.body);
    });
}
module.exports = { // MUY IMPORTANTE PARA VER LAS REFERENCIAS
    cambioEstado,
    getUserInfo: getUserInfo
}