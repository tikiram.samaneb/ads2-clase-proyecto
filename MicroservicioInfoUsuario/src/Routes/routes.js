var express = require('express');
var router = express.Router(); // para manejar todas las rutas que entren al servidor

const users_controller = require('../Controller/controller')

router.get ('/getUserInfo/user', users_controller.getUserInfo); 
router.get ('/activar', users_controller.cambioEstado); 

module.exports = router;