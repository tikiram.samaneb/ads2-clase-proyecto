


## Endpoints

### New Agent

```
POST http://localhost:3001/agents
Content-Type: application/json

{
  "firstName": "Josepe",
  "lastName": "Escobar",
  "password": "12341234",
  "position": "agent II"
}
```

#### Response Example

```json
{
  "id": 3,
  "firstName": "Josepe",
  "lastName": "Escobar",
  "password": "12341234",
  "position": "agent II",
  "updatedAt": "2020-10-28T03:15:34.163Z",
  "createdAt": "2020-10-28T03:15:34.163Z",
  "photo_url": null
}
```

### Get all agents

```
GET http://localhost:3001/agents
```

#### Response Example:
```json
[
  {
    "id": 2,
    "firstName": "Josepe",
    "lastName": "Escobar",
    "password": "12341234",
    "position": "agent II",
    "photo_url": null,
    "createdAt": "2020-10-28T03:05:54.911Z",
    "updatedAt": "2020-10-28T03:05:54.911Z"
  }
]
```

### Get agent

```
GET http://localhost:3001/agents/2
```

#### Response Example:
```json
  {
    "id": 2,
    "firstName": "Josepe",
    "lastName": "Escobar",
    "password": "12341234",
    "position": "agent II",
    "photo_url": null,
    "createdAt": "2020-10-28T03:05:54.911Z",
    "updatedAt": "2020-10-28T03:05:54.911Z"
  }
```

### Update Usuario

```
PATCH http://localhost:3001/agent/2
Content-Type: application/json

{
  "firstName": "Otro Nombre"
}
```

Response status code: 204 - No Content

### Delete agent

```
DELETE http://localhost:3001/agent/1
Content-Type: application/json
```

Response status code: 204 - No Content
