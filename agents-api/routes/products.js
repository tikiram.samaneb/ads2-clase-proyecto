const express = require('express');
const { models } = require('./../conn');
const router = express.Router();

/* GET users listing. */
router.get('/', async function(req, res) {
    try {
        const agents = await models.agent.findAll();
        res.status(200).json(agents);
    }
    catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message });
    }
});

router.get("/:id", async (req, res) => {
    try {
        const id = req.params.id;
        if (id === undefined) {
            res.status(400).json({ message: 'A agent id is required' });
            return;
        }

        const agent = await models.agent.findByPk(id);
        res.status(200).json(agent);
    }
    catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const newAgent = await models.agent.create(req.body);
        res.status(200).json(newAgent);
    }
    catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        if (id === undefined) {
            res.status(400).json({ message: 'A agent id is required' });
            return;
        }

        await models.agent.destroy({
            where: {
                id,
            }
        });

        res.status(204).send();
    }
    catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message });
    }
});

router.patch('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        if (id === undefined) {
            res.status(400).json({ message: 'A agent id is required' });
            return;
        }

        await models.agent.update(req.body, {
            where: {
                id
            }
        });

        res.status(204).send();
    }
    catch (e) {
        console.error(e);
        res.status(500).json({ message: e.message });
    }
});

module.exports = router;
