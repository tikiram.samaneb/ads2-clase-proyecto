const chai = require('chai');
const chaiHttp = require('chai-http');

const url = 'http://localhost:3001';
chai.use(chaiHttp);
chai.should();

describe("Productos", () => {

    describe('POST /products', () => {
        it('debería crear un producto', (done) => {
            let testProduct = {
                name: "Test Product",
                photo_url: " ",
                price: "0"
            }
            chai.request(url)
                .post('/products')
                .send(testProduct)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
        it('no debería crear un producto', (done) => {
            let testProduct = {
                name: "Test Product",
            }
            chai.request(url)
                .post('/products')
                .send(testProduct)
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('GET /products', () => {
        it("deberia de mostrar todos los productos que se tienen", (done) => {
            chai.request(url).get('/products')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('DELETE /products/:id', () => {
        it('no debería permitir eliminar ningun producto', (done) => {
            const id = 8889;
            chai.request(url)
                .delete(`/products/${id}`)
                .send({})
                .end((err, res) => {
                    res.should.have.status(204);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

});
