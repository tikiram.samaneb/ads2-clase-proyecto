const { Sequelize, DataTypes } = require('sequelize');


// DATABASE_URL=postgres://{user}:{password}@{hostname}:{port}/{database-name}

const user = 'postgres';
const password = 'admin';
const hostname = 'postgres-container';
const port = 5432;
const database = 'postgres';

const DATABASE_URL = `postgres://${user}:${password}@${hostname}:${port}/${database}`
console.log('DB URL: ', DATABASE_URL);

// antiguo
// const DATABASE_URL = 'postgres://saqsfimd:VgsC5qFO-6GFGxVKRMT3MO8ywph3xXow@rajje.db.elephantsql.com:5432/saqsfimd';

// Conexion a la base de datos

const sequelize = new Sequelize(DATABASE_URL);

sequelize.define('agent', {
    // The following specification of the 'id' attribute could be omitted
    // since it is the default.
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    firstName: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    lastName: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    password: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    position: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    photo_url: {
        allowNull: true,
        type: DataTypes.STRING
    },
});

module.exports = sequelize;
