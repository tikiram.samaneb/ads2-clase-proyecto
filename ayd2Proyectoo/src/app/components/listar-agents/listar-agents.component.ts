import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AgenteService } from '../../services/agente.service';
import { UpdateComponent } from '../update/update.component';
import { DeleteAgentsComponent } from '../delete-agents/delete-agents.component';

@Component({
  selector: 'app-listar-agents',
  templateUrl: './listar-agents.component.html',
  styleUrls: ['./listar-agents.component.css'],

})
export class ListarAgentsComponent implements OnInit {

  constructor(private AgenteService:AgenteService , private router: Router) { }

  ngOnInit(): void {
     this.listarAgentes();
  }


  lstAgentes:any = [];
  idUsu:number = 0;


  listarAgentes(){

    this.AgenteService.listarAgente().subscribe(
      res => {
        this.lstAgentes = res;
        console.log(res);
      },
      err => console.log(err)
    );

  }

  irModificar(i:number){
    this.idUsu = i;
    const val = this.idUsu + 1;
    console.log(`ID A MODIFICAR ${val}`);
    localStorage.setItem("idAgente",val.toString());
    this.router.navigate(['updateUsuario']);
  }

  irEliminar(i:number){
    this.idUsu = i;
    const val = this.idUsu + 1;
    console.log(`ID A ELIMINAR ${val}`);
    //localStorage.setItem("idAgente",val.toString());
    //this.router.navigate(['delete']);
    
    this.AgenteService.deleteAgente(val.toString()).subscribe(
      res => {
        this.listarAgentes();
      },
      err => console.log(err)
    );
  
  }


  irObtener(i:number){
    this.idUsu = i;
    const val = this.idUsu + 1;
    console.log(`ID A OBTENER DATOS ${val}`);
    localStorage.setItem("idAgente",val.toString());
    this.router.navigate(['obtener']);
  
  }

  irInicio(){
    this.router.navigate(['inicio']);
  }

}
