import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAgentsComponent } from './listar-agents.component';

describe('ListarAgentsComponent', () => {
  let component: ListarAgentsComponent;
  let fixture: ComponentFixture<ListarAgentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarAgentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
