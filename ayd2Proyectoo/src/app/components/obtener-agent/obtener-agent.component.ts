import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../models/usuario';
import { AgenteService } from '../../services/agente.service';
@Component({
  selector: 'app-obtener-agent',
  templateUrl: './obtener-agent.component.html',
  styleUrls: ['./obtener-agent.component.css']
})
export class ObtenerAgentComponent implements OnInit {

  constructor(private agenteServices: AgenteService, private router: Router) { 
    this.obterDatos();
  }

  usuario: Usuario = {
    id:0,
    firstName: "",
    lastName: "",
    password: "",
    position: "",
    updatedAt: "2020-10-28T03:15:34.163Z",
    createdAt: "2020-10-29T05:42:23.221Z",
    photo_url: null
  };
  ngOnInit(): void {
  }

  obterDatos(){
    const val = localStorage.getItem("idAgente");
    this.agenteServices.obtenerAgente(val.toString()).subscribe((res:any) => {
        console.log(res);
        this.usuario.id = res.id;
        this.usuario.firstName = res.firstName;
        this.usuario.lastName = res.lastName;
        this.usuario.position = res.position;

      },
      err => console.log(err)
    );
  }

  irInicio(){
    this.router.navigate(['listar']);
  }
}
