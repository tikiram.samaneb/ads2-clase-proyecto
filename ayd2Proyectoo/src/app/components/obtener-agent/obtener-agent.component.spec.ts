import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObtenerAgentComponent } from './obtener-agent.component';

describe('ObtenerAgentComponent', () => {
  let component: ObtenerAgentComponent;
  let fixture: ComponentFixture<ObtenerAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObtenerAgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObtenerAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
