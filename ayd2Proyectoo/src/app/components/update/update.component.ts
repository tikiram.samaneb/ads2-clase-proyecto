import { Component, OnInit, ɵConsole } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../../models/usuario'
import { AgenteService } from '../../services/agente.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  imageError: string;
  seModifico: boolean;
  cardImageBase64: string;

  
  usuario: Usuario = {
    id:0,
    firstName: "",
    lastName: "",
    password: "",
    position: "",
    updatedAt: "2020-10-28T03:15:34.163Z",
    createdAt: "2020-10-29T05:42:23.221Z",
    photo_url: null
  };

  datosObligarios: boolean = true;
  actualizado: boolean = false;
  valor:number = 0;
  constructor(private AgenteService: AgenteService, private router: Router) { 
    
  }

  ngOnInit(): void {
   // if (localStorage.getItem("id_usuario") === null) { this.router.navigate(['login']); }
    //this.obtenerDatos();
    //this.usuario.id_usuario=localStorage.getItem("id_usuario");
    console.log(`VALOREEEEEEEEEEEEEEER ${localStorage.getItem("idAgente")}`);
    this.obterDatos();
  }

  update() {
    const val = localStorage.getItem("idAgente");
    console.log(`ID MODIFICAR EN UPDATE ${val}`);
    
    this.AgenteService.updateAgente(val.toString(),this.usuario).subscribe((res:any) => {
      console.log(res);
      this.usuario.id = 0;
      this.usuario.firstName = "";
      this.usuario.lastName = "";
      this.usuario.position = "";
      this.seModifico = true;
    },
    err => console.log(err)
  );
  
    this.seModifico = false;
  }

  obterDatos(){
    const val = localStorage.getItem("idAgente");
    this.AgenteService.obtenerAgente(val.toString()).subscribe((res:any) => {
        console.log(res);
        this.usuario.id = res.id;
        this.usuario.firstName = res.firstName;
        this.usuario.lastName = res.lastName;
        this.usuario.position = res.position;

      },
      err => console.log(err)
    );
  }
  
  irInicio(){
    this.router.navigate(['listar']);
  }


}
