import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../../services/login-service.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  constructor(private logoutUser:LoginServiceService, private router:Router) { }


  ngOnInit(): void {
    
  }

  irInicio(){
    this.router.navigate(['inicio']);
  }



  

}
