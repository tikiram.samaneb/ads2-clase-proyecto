import { Component, OnInit, HostBinding , Input } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Usuario } from '../../models/usuario';
import { AgenteService } from '../../services/agente.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  @HostBinding('class') clases = 'row';

  usuario: Usuario = {
    id:0,
    firstName: "",
    lastName: "",
    password: "",
    position: "",
    updatedAt: "2020-10-28T03:15:34.163Z",
    createdAt: "2020-10-29T05:42:23.221Z",
    photo_url: null
  };
  lstAgentes:any = [];
  edit: boolean = false;
  usuarioError : boolean = false;
  rpas: boolean = false;
  tmp:number = 0;
  @Input() idAgente: number;

  constructor(private agenteServices: AgenteService, private router: Router) { }

  ngOnInit(): void {
     console.log(`ID USUARIO ELEGIDO ${this.idAgente}`);
     this.obterDatos();
  }

  saveNewUser(){
    
    if(this.usuario.lastName === ''  || this.usuario.firstName === '' || this.usuario.password === '' || this.rpas === false){
      this.usuarioError = true;
    } else {
      this.usuarioError = false;
      
      this.agenteServices.crearAgente(this.usuario).subscribe(
        res => {
          console.log(res);
          this.usuario.id = 0;
          this.usuario.firstName = "";
          this.usuario.lastName = "";
          this.usuario.position = "";
        },
        err => console.log(err)
      );
      
    }
  }

  verificar(dato: HTMLInputElement){
    if(dato.value !== this.usuario.password){
      dato.value = '';
      return this.rpas = false;
    } 

    return this.rpas = true;

  }


  obterDatos(){
    this.agenteServices.listarAgente().subscribe(
      res => {
        this.lstAgentes = res;
        console.log(res);
      },
      err => console.log(err)
    );
  }

    irInicio(){
      this.router.navigate(['inicio']);
    }
}
