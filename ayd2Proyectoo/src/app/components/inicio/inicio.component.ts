import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Router } from '@angular/router';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {


  constructor(private router: Router) { }

  ngOnInit(): void {
 
  }

  
  irEliminar(){
    this.router.navigate(['delete']);
  }

  irListar(){
    this.router.navigate(['listar']);
  }

  irRegistrar(){
    this.router.navigate(['registro']);
  }

  irModificar(){
    this.router.navigate(['updateUsuario']);
  }

  irObtener(){
    this.router.navigate(['obtener']);
  }

}
