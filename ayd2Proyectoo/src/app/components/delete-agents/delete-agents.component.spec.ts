import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAgentsComponent } from './delete-agents.component';

describe('DeleteAgentsComponent', () => {
  let component: DeleteAgentsComponent;
  let fixture: ComponentFixture<DeleteAgentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAgentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
