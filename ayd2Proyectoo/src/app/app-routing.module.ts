import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { UpdateComponent } from './components/update/update.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { DeleteAgentsComponent } from './components/delete-agents/delete-agents.component';
import { ListarAgentsComponent } from './components/listar-agents/listar-agents.component';
import { ObtenerAgentComponent } from './components/obtener-agent/obtener-agent.component';
const routes: Routes = [
    
    {
      path:'registro',
      component:RegistroComponent
    },
    {
      path:'updateUsuario',
      component:UpdateComponent
    },
    {
      path:'',
      component:InicioComponent
    },
    {
      path:'inicio',
      component:InicioComponent
    },
    {
      path:'delete',
      component:DeleteAgentsComponent
    },
    {
      path:'listar',
      component:ListarAgentsComponent
    },
    {
      path:'obtener',
      component:ObtenerAgentComponent
    }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingCompontent = [LoginComponent, RegistroComponent]