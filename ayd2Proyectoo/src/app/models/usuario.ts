import { Title } from '@angular/platform-browser';

export interface Usuario {
    id:number,
    firstName: string,
    lastName: string,
    password: string,
    position?: string,
    updatedAt?: string,
    createdAt?: string,
    photo_url?: string
  
};
