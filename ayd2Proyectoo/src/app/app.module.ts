import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule , routingCompontent} from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistroComponent } from './components/registro/registro.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UpdateComponent } from './components/update/update.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ListarAgentsComponent } from './components/listar-agents/listar-agents.component';
import { DeleteAgentsComponent } from './components/delete-agents/delete-agents.component';
import { ObtenerAgentComponent } from './components/obtener-agent/obtener-agent.component';

@NgModule({
  declarations: [
    AppComponent,
    routingCompontent,
    RegistroComponent,
    NavbarComponent,
    UpdateComponent,
    InicioComponent,
    ListarAgentsComponent,
    DeleteAgentsComponent,
    ObtenerAgentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
