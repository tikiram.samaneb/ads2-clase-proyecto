import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';
import { ipApp } from '../models/host';
@Injectable({
  providedIn: 'root'
})
export class AgenteService {

  API_URI = ipApp;
  
  constructor(private http: HttpClient) { }

   crearAgente(usuario:Usuario){
      return this.http.post(`${this.API_URI}/agents`, usuario);
   }

   updateAgente(id:string, usuario:Usuario){
    return this.http.patch(`${this.API_URI}/agents/${id}`, usuario);
   }

   deleteAgente(id:string){
    return this.http.delete(`${this.API_URI}/agents/${id}`);
   }

   obtenerAgente(id:string){
    return this.http.get(`${this.API_URI}/agents/${id}`);
   }

   listarAgente(){
     return this.http.get(`${this.API_URI}/agents`);
   }
}
