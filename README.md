# ADS2 - Practica 1

## Docker Compose

Levantar todo

```bash
sudo docker-compose up
```

### Base de Datos (Fragmento de `docker-compose.yml`)

Levanta un container con Postgres, con usuario `postgres`, password `admin`, y base de datos `postgres`.

Tiene asignado un volumen en `$HOME/docker/volumes/postgres`, Si estan usando windows miren como se comporta eso.

```yml
  practica-postgres:
    image: "postgres:12.4"
    container_name: postgres-container
    volumes:
      - $HOME/docker/volumes/postgres:/var/lib/postgresql/data
    ports:
      - 5432:5432
    environment:
      POSTGRES_PASSWORD: "admin"
```

#### Acceso a DB desde otros containers

* **Si estan haciendo un proyecto fuera de un container, pueden hacer referencia a la db con `localhost:5432`**
* Si se quiere acceder desde otro container debe de ser `postgres-container:5432`
* Por lo anterior mencionado se recomienda pasar el nombre del host por una variable de entorno (en agents-api no fue usado eso).

### agents-api

Se levanta el servicio en **`localhost:3001`**.
